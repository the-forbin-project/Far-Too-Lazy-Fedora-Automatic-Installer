#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root or run script with sudo..."
  exit
fi

echo "*-------------------------------------*"
echo "| FAR TOO LAZY: FEDORA AUTO INSTALLER |"
echo "|         WORKSTATION EDITION         |"
echo "| 	Built for Fedora 29	    |"
echo "*-------------------------------------*"
echo "        PRESS ENTER TO CONTINUE"

read uselessVar
clear

echo "THIS ONLY WORKS FOR FEDORA 29 OR HYPOTHETICALLY ANY DNF BASED SYSTEM"
echo "USUAL WARNINGS HERE, USE IT ON MY SYSTEM DON'T WORRY BUT STILL CHECK SOURCE CODE ;)"
echo ""
echo "YOU'LL HAVE TO MANUALLY START CLION.SH Command to do so [no sudo needed]: $~/IDEs/clion-2018.3.3/bin/clion.sh "
echo "CHANGE OR MODIFY IF WANTED" 
echo "PRESS ENTER TO CONTINUE"
read uselessVar

####################################
## FIRST GOT TO CHECK FOR UPDATES ##
####################################
echo "CHECKING FOR UPDATES FIRST!"

echo "GO MAKE SOME TEA OR COFFEE I'LL TAKE IT FROM HERE!"
sudo dnf upgrade -y

##############################
## Time To Install Packages ##
##############################
# IF WANTING TO NOT INSTALL THESE COMMENT OUT
# TO ADD JUST ADD YOUR INSTALL COMMAND FOLLOWED BY A -y

echo "INSTALLING PACKAGES..."
sleep 3

sudo dnf install neofetch -y
sudo dnf install eclipse -y
sudo dnf install android-tools -y
sudo dnf install htop -y
sudo dnf install nano -y
sudo dnf install gnome-tweak-tool -y

#############################
## NON FREE PACKAGES BELOW ##
#############################

echo "ENABLING RPM-FUSION REPOS"
sleep 3

sudo rpm -ivh https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-29.noarch.rpm

sudo rpm -ivh https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-29.noarch.rpm

########################################################
## ENABLING FLATHUB & INSTALLING GITKRAKEN & DISCORD  ##
########################################################
echo "ENABLING FLATHUB & INSTALL GITKRAKEN"
sleep 3
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.axosoft.GitKraken
flatpak install flathub com.discordapp.Discord
#flatpak install flathub com.slack.Slack #In case people want slack. I don't use it.

#####################################
## MANUALLY INSTALL SOME THINGS :( ##
#####################################
echo "DOWNLOADING, UNPACKING AND STARTING CLION"
sleep 3
mkdir IDEs
cd IDEs
wget https://download.jetbrains.com/cpp/CLion-2018.3.3.tar.gz
tar -xvzf CLion-2018.3.3.tar.gz
cd ..
chmod 755 clion-2018.3.3
cd ..
chmod 755 IDEs
################################
## DOING SOME... ENHANCEMENTS ##
################################
echo "Faster IO Speeds!"
sleep 3

ssd_scheduler="deadline" #change this to whatever you want.
hdd_scheduler="deadline" #same as above I just like deadline

cat <<EOF | tee "/etc/udev/rules.d/60-io_schedulers.rules" > /dev/null 2>&1
# Set deadline scheduler for non-rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="$ssd_scheduler"
# Set deadline scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="$hdd_scheduler"
EOF

for disk in /sys/block/sd*; do
    rot="$disk/queue/rotational"
    sched="$disk/queue/scheduler"

    if [[ $(cat "$rot") -eq 0 ]]; then
        echo "$ssd_scheduler | tee $sched > /dev/null 2>&1"
    elif [[ $(cat "$rot") -eq 1 ]]; then
        echo "$hdd_scheduler | tee $sched > /dev/null 2>&1"
    fi
done

##############################
##   CLEANING UP/REBOOTING  ##
##############################
clear
echo "CLEANING AND REMOVING TEMP FILES..."
sudo dnf clean all

echo "REBUILDING REPOSITORIES, THIS MAY TAKE SOME TIME..."
sudo dnf update

clear && neofetch && echo "FINISHED! DO YOU WANT TO REBOOT?(y/n)"
read answer

if [[ $answer = "y" ]]
then
sudo reboot
fi






